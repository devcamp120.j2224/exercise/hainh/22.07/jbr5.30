package com.example.demo.Service;

import java.util.ArrayList;


import org.springframework.stereotype.Service;


import com.example.demo.Model.Book;

@Service
public class BookService {
    
    public static ArrayList<Book> getAllBookList() {
        ArrayList<Book> bookList = new ArrayList<Book>();
        
        Book book1 = new Book("trại súc vật" , 2000.00 , 2000 );
        Book book2 = new Book("cha giàu cha nghèo",5000.000,5000 );
        Book book3 = new Book("sống làm chi",100000.0000 , 10000 );

        book1.setAuthors(AuthorService.getAuthorVN());
        book2.setAuthors(AuthorService.getAuthorJP());
        book3.setAuthors(AuthorService.getAuthorUS());

        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);


        return bookList;

        }
    }
          
