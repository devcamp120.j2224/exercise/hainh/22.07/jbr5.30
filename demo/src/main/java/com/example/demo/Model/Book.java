package com.example.demo.Model;

import java.util.ArrayList;

public class Book {
    
    private String name ;
    private ArrayList<Author> authors ;
    private double price ;
    private int qty ;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public ArrayList<Author> getAuthors() {
        return authors;
    }
    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public int getQty() {
        return qty;
    }
    public void setQty(int qty) {
        this.qty = qty;
    }

    public Book() {

    }

    public Book(String name, double price , int qty ) {
        this.name = name;
        this.qty = qty;
        this.price = price;
    }
    
    public Book(String name, double price, int qty , ArrayList<Author> authors) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }
 
}
